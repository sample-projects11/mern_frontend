export const SliderItems=[
{
    id:1,
    img:"shopping.png",
    title:"Summer Sale",
    Des:"DONT COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
    bg:"f5fafd",
},{
    
    id:2,
    img:"shopping.png",
    title:"Winter Sale",
    Des:"DONT COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS",
    bg:"fcf1ed",
}
,{
    
    id:3,
    img:"shopping.png",
    title:"Popular Sale",
    Des:"DONT COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS",
    bg:"fbf0f4",
}]

export const Categorie=[
    {
        id:1,
        img:"https://img.freepik.com/free-photo/stylish-muscular-confident-male-posing-attractive-handsome-caucasian-guy-with-trendy-haircut-with-black-leather-jacket-thrown-shoulder-holding-sunglasses-hand_176420-13371.jpg?t=st=1651039701~exp=1651040301~hmac=3515c91e3b915c35690e606eb39998bf9cfd5e18644d5673784b2614dda8240d&w=740",
        title:"SHIRT STYLE",
        cat:"women"
       
    },{
        
        id:2,
        img:"https://img.freepik.com/free-photo/happy-couple-laughing-traveling-summer-by-sea-man-woman-wearing-sunglasses_285396-725.jpg?t=st=1651039793~exp=1651040393~hmac=a671c10f63f40c4f2e9ba53dafa90463b65ee62772b8e2f5a3e716775a3ad782&w=740",
        title:"LOUNGE WEAR LOVE",
        cat:"jeans"
    }
    ,{
        
        id:3,
        img:"https://img.freepik.com/free-photo/smiling-touching-arms-crossed-room-hospital_1134-799.jpg?t=st=1651039824~exp=1651040424~hmac=9918c74311a4c30a01d1baadfd6ceeb80e4451c1d5ffec82ef0de78891845afe&w=740",
        title:"LIGHT JACKETS",
        cat:"jeans"
       
    }]

    export const PopularProducts=[
        {
            
            id:1,
            img:"Products/tshirt-removebg-preview.png",
            
        }
        ,{
            
            id:2,
            img:"Products/jacket-removebg-preview.png",
            
           
        },
        {
            id:3,
            img:"Products/white-removebg-preview.png",
            
           
        },{
            
            id:4,
            img:"Products/tshirt-removebg-preview.png",
            
        }
        ,{
            
            id:5,
            img:"Products/jacket-removebg-preview.png",
            
           
        },
        {
            id:6,
            img:"Products/white-removebg-preview.png",
            
           
        },
        {
            
            id:5,
            img:"Products/jacket-removebg-preview.png",
            
           
        },
        {
            id:6,
            img:"Products/white-removebg-preview.png",
            
           
        },]