import styled from '@emotion/styled'
import React from 'react'

const Container =styled.div`
    height:30px;
    background-color:teal;
    color:white;
    display:flex;
    align-items:center;
    justify-content:center;
    font-size:14px;
    font-weight:bold;
`

function Announcement() {
  return (
    <div>
        <Container>Super Deal! Free Shopping on orders over $50</Container>
    </div>
  )
}

export default Announcement