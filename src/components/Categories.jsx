import React from 'react'
import styled from '@emotion/styled'
import { Categorie } from '../data'
import CategoryItem from './CategoryItem'
import {mobile} from "../Responsive";



const Container=styled.div`
display:flex;
padding:20px;
justify-content:space-between;
${mobile({padding:"0 px",flexDirection:"column"})}

`

function Categories() {
  return (
    <Container>{Categorie.map(item=>(
        <CategoryItem item={item} key={item.id}/>
    ))}</Container>
  )
}

export default Categories