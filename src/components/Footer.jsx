import styled from '@emotion/styled'
import React from 'react'
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import PinterestIcon from '@mui/icons-material/Pinterest';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import EmailIcon from '@mui/icons-material/Email';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import {mobile} from "../Responsive"

const Container=styled.div`
display:flex;
${mobile({flexDirection:"column"})}

`


const Left=styled.div`
flex:1;
display:flex;
flex-direction:column;
padding:20px;`

const Logo=styled.h1``

const Desc=styled.p`
margin:20px 0px;
`

const SocialContainer=styled.h1`
display:flex;`

const SocialIcon=styled.h1`
width:40px;
height:40px;
border-radius:50%;
color:white;
background-color:#${props=>props.color};
display:flex;
align-items:center;
justify-content:center;
margin-right:20px;
`

const Title=styled.h3`
margin-bottom:30px;
`
const List=styled.ul`
margin:0;
padding:0;
list-style:none;
display:flex;
flex-wrap:wrap;
`

const Listitem=styled.li`
width:50%;
margin-bottom:10px;
`

const Center=styled.div`
flex:1;
padding:20px;
${mobile({backgroundColor:"#eee"})}

`

const Right=styled.div`
flex:1;
padding:20px;
${mobile({display:"none"})}
`

const ContactItem=styled.div`
margin-bottom:20px;
display:flex;
align-items:center;
`


function Footer() {
  return (
    <Container>
        <Left>
            <Logo>CART.</Logo>
            <Desc>
In publishing and graphic design, 
Lorem ipsum is a placeholder text commonly used 

</Desc>
<SocialContainer>
<SocialIcon  color='385999' >
    <FacebookIcon/>
</SocialIcon>
<SocialIcon color='E4405F'>
    <InstagramIcon/>
</SocialIcon>
<SocialIcon color='55ACEE'>
    <TwitterIcon/>
</SocialIcon>
<SocialIcon color='E60023'>
    <PinterestIcon/>
</SocialIcon>
</SocialContainer>

        </Left>
        <Center>
            <Title>Usefull Links</Title>
            <List>
            <Listitem>Home</Listitem>
            <Listitem>Cart</Listitem>
            <Listitem>Men Fashion</Listitem>
            <Listitem>Accessories</Listitem>
            <Listitem>My Account</Listitem>
            <Listitem>Order Tracking</Listitem>
            <Listitem>WishList</Listitem>
            <Listitem>Terms</Listitem>
            </List>
  
        </Center>
        <Right><Title>Contact</Title>
        <ContactItem><LocationOnIcon style={{marginRight:"10px"}}/>622 Dixie path, South Tobister 98366</ContactItem>
        <ContactItem><LocalPhoneIcon style={{marginRight:"10px"}}/>+1  98524 55 54 </ContactItem>
        <ContactItem><EmailIcon style={{marginRight:"10px"}}/>contact@info.com </ContactItem>


        </Right>

    </Container>
  )
}

export default Footer