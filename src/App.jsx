import Product from "./pages/Product";
import Home from "./pages/Home";
import ProductList from "./pages/ProductList";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Cart from "./pages/Cart";
import Success from "./pages/Success";
import {
  Routes,
  Route,
  Link,
} from "react-router-dom";


const App = () => {
  const user=true;
  return <div>
      <Routes>
      <Route exact path="/" element={<Home/>}/>
       <Route path="/products/:category" element={<ProductList/>}/>
       <Route path="/Product/:id" element={<Product/>}/>
       <Route path="/cart/" element={<Cart/>}/>
       <Route path="/success" element={<Success/>}/>
       <Route path="/login" element={<Login/>}/>
       <Route path="/register" element={<Register/>}/>
    </Routes>
   

  </div>;
};

export default App;