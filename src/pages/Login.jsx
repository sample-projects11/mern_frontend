import React, { useState } from 'react'
import styled from '@emotion/styled'
import {mobile} from "../Responsive"
import { login } from '../redux/apiCalls'
import { useDispatch ,useSelector} from 'react-redux'



const Container=styled.div`
width:100vw;
height:100vh;
background:url("https://img.freepik.com/free-photo/arabic-lamp-with-colorful-light-with-colored-background_9083-5637.jpg?w=740")center;
display:flex;
align-items:center;
justify-content:center;
background-size:cover;
`


const Wrapper=styled.div`
padding:20px;
width:25%;
background-color:white;
${mobile({width:"75%"})}

`
const Title=styled.h1`
font-size:24px;
font-weight:300;
`
const Form=styled.form`
display:flex;
flex-direction:column;
`
const Input=styled.input`
flex:1;
min-width:40%;
margin:10px 0;
padding:10px;
`


const Button=styled.button`
width:40%;
border:none;
padding:15px 20px;
background-color:teal;
color:white;
cursor:pointer;
margin-bottom:10px;
&::disabled{
  color:green;
  cursor:not-allowed;
}
`

const Link=styled.a`
margin:5px 0px;
font-size:12px;
text-decoration:underline;
cursor:ponter;
`

const Error =styled.span`
  color:red;
`

function Login() {
  const [username,setUsername] = useState("")
  const [password,setPassword] = useState("")
  const dispatch= useDispatch()
  const {isFetching,error} =useSelector((state)=>state.user)

  const handleClick = (e)=>{
    e.preventDefault()
    login(dispatch,{username,password})
  }
  return (
    
    <Container>
      <Wrapper>
        <Title>
          SIGN IN
        </Title>
        <Form>
          <Input placeholder="USERNAME" 
          onChange={(e)=>setUsername(e.target.value)}/>
          <Input placeholder="PASSWORD" type="password" 
          onChange={(e)=>setPassword(e.target.value)}/>
          
          
<Button onClick={handleClick} disabled={isFetching}>LOGIN</Button>
{error && <Error>Something went wrong</Error>}
<Link>Forgot password?</Link>
<Link>Create a new account?</Link>
        </Form>
      </Wrapper>
    </Container>
  )
}

export default Login