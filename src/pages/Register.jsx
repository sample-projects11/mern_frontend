import React from 'react'
import styled from '@emotion/styled'
import {mobile} from "../Responsive"


const Container=styled.div`
width:100vw;
height:100vh;
background:url("https://img.freepik.com/free-photo/gray-background-with-palm-branch_172251-2247.jpg?w=740")center;
display:flex;
align-items:center;
justify-content:center;
background-size:cover;
`


const Wrapper=styled.div`
padding:20px;
width:40%;
background-color:white;
${mobile({width:"75%"})}

`
const Title=styled.h1`
font-size:24px;
font-weight:300;
`
const Form=styled.form`
display:flex;
flex-wrap:wrap;
`
const Input=styled.input`
flex:1;
min-width:40%;
margin:20px 10px 0px 0px;
padding:10px;
`
const Aggrement=styled.span`
font-size:12px;
margin:20px 0px;
`

const Button=styled.button`
width:40%;
border:none;
padding:15px 20px;
background-color:teal;
color:white;
cursor:pointer;
`

function Register() {
  return (
    <Container>
      <Wrapper>
        <Title>
          CREATE AN ACCOUNT
        </Title>
        <Form>
          <Input placeholder="Name.."/>
          <Input placeholder="Last name"/>
          <Input placeholder="User name"/>
          <Input placeholder="Email"/>
          <Input placeholder="Password"/>
          <Input placeholder="Confirm Password"/>
          <Aggrement>By Creating an account , I Consent to processing of my personal data accordance with the <b>PRIVACY AND POLICY</b></Aggrement>
<Button>CREATE</Button>
        </Form>
      </Wrapper>
    </Container>
  )
}

export default Register